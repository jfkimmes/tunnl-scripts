# Tunnl Scripts
A collection of easily auditable scripts that make it easy to set up (semi-)publicly reachable reverse SSH port forwards to services on your LAN.

*This is under active development and not ready to be used in production.*

<!--

## Usecases / Ideas
### Dev
- Share a local dev server with someone on the internet.
- Securly expose a service on your local network to a set of hosts on the internet.

### Pentesting
- Reach a reverse shell in a constrained network environment from the outside.
- Make blind injections visible by pinging your public web service.

# Setup

## Manual
TODO


## Ansible
You need:
- Ansible
- a domain with access to DNS records
- a minimal VM with root privileges

### DNS
Set a wildcard `A` (and/or `AAAA`) record for `*.<example.com>` to the IP of your VM.

### Server
- set up VM with sudo-able ssh user
- harden the VM
- change `BASE_DOMAIN` in `server/add_tunnl.sh` and in `server/clean_tunnl.sh`
- run the playbook
- todo

### Client
- change `SERVER_IP` to your domain/ip.
- todo
-->
