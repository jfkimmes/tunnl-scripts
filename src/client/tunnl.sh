#!/bin/bash

SERVER_IP=joerd

_term () { 
  echo -e "\n[+] Cleaning up..." 
  echo -e "\t[*] Killing SSH Daemon." 
  kill -TERM "$child" 2>/dev/null
  echo -e "\t[*] Cleaning up server." 
  ssh tunnl@${SERVER_IP} "sudo ./clean_tunnl.sh $url"
  echo -e "[+] Done."
}

print_help () {
    echo -e "Tunnl facilitates setting up reverse port forwards,"
    echo -e "in order to expose local services to the internet."
    echo -e " "
    echo -e "Usage:\n======"
    echo -e "tunnl -p <port> [-d <domain>] [-s <source(s)>]"
    echo -e " "
    echo -e "-p:\t the port you want to expose."
    echo -e "-d:\t (optional) a personal subdomain where everyone \n\t  can find your forwarded port."
    echo -e "-s:\t (optional) a comma separated list of source IP \n\t  addresses that are allowed to access the domain."
    echo -e "\n"
    echo -e "Examples: \n========="
    echo -e "tunnl -p 4444 \n\tTunnels ydk3b.tunnl.eu to localhost:4444."
    echo -e "tunnl -p 4444 -d foo \n\tTunnels foo.tunnl.eu to localhost:4444."
    echo -e "tunnl -p 4444 -d foo -s 111.222.333.444 \n\tTunnels foo.tunnl.eu to localhost:4444 \n\tand allows only host with IP 111.222.333.444 \n\tto acccess it."

}

clean_up () {
    ssh tunnl@${SERVER_IP} "sudo ./clean_tunnl.sh ALL"
    if [ $? -ne 1 ]; then
        echo "[-] Could not clean up."
        exit 1
    fi
}

trap _term SIGTERM
trap _term SIGINT

if [ $# -lt 1 ]; then
    print_help
    exit 1
fi

while getopts "h?p:d:c:" opt; do
    case "$opt" in 
        h|\?)
            print_help
            exit 0
            ;;
        p)
            targetPort=$OPTARG
            ;;
        d)  
            vanitydomain=$OPTARG
            # TODO: user vanity url
            ;;
        c)
            if [ $OPTARG != "ALL" ]; then
                print_help
                exit 1
            fi
            clean_up
            exit 0
            ;;
    esac
done
shift $((OPTIND-1))
[ "${1:-}" = "--" ] && shift

resultString=$(ssh tunnl@${SERVER_IP} "sudo ./add_tunnl.sh") > /dev/null 2>&1 
resultCode=$?
if [ $resultCode -ne 0 ]; then
    echo "[-] Could not create new tunnl."
    exit 1
fi
sourcePort=$(echo $resultString | awk -F' ' '{print $2}')
url=$(echo $resultString | awk -F' ' '{print $5}')
echo -e "[+] Successfully added new tunnl on internal port $sourcePort."

echo -e "[+] Connecting..."
ssh -N tunnl@${SERVER_IP} -R ${sourcePort}:localhost:${targetPort} &
child=$! 
echo -e "[+] Connected."
echo -e "\t [*] $url"
wl-copy $url
echo -e "\t [*] Added url to your clipboard."

wait "$child"
