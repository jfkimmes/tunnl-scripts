#!/bin/bash

BASE_DOMAIN="tunnl.eu"

validateDomain() {
    if [[ $1 =~ ^[a-zA-Z0-9]+$ ]]; then
        echo $1
    else
      echo ""
    fi
}

if [ $# -ne 1 ]; then
    exit 1
fi

input=$(validateDomain $1)


if [ $input == "ALL" ]; then
    rm /etc/nginx/sites-available/*_tunnl
    rm /etc/nginx/sites-enabled/*_tunnl
else
    input=$(echo $input | sed 's/http:\/\///g')
    input=$(echo $input | sed "s/\.${BASE_DOMAIN}\///g")
    
    rm /etc/nginx/sites-available/${input}_tunnl
    rm /etc/nginx/sites-enabled/${input}_tunnl
fi
