#!/bin/bash

#
# Usage: add_tunnl.sh 
#   -d <domain>             Specify a desired subdomain
#   -s <IP>                 Allow only connections from a specific source IP 
#

BASE_DOMAIN="tunnl.eu"
RANDOM_PORT=$(python3 -c 'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()')

validateIp() {
    if [[ $1 =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        echo $1
    else
      echo ""
    fi
}

validateDomain() {
    if [[ $1 =~ ^[a-zA-Z0-9]+$ ]]; then
        echo $1
    else
      echo ""
    fi
}

while getopts d:s: OPTION
do
    case $OPTION in
        d) subdomain=$(validateDomain ${OPTARG});;
        s) sourceIp=$(validateIp ${OPTARG});;
    esac
done


# if user input is not provided or not valid, use random value
randomString=$(tr -dc a-z0-9 </dev/urandom | head -c 5 ; echo '')
if [ $subdomain = "" ]; then
    domainString=$randomString
else
    domainString=$subdomain
fi

# copy template to new tunnl config
filename=${domainString}_tunnl
cd /etc/nginx/sites-available
cp ./template ./$filename

# replace placeholders in template file
sed -i "s/X/${domainString}.${BASE_DOMAIN}/g" ./$filename
sed -i "s/Y/${RANDOM_PORT}/g" ./$filename
if [ ! $sourceIp = "" ]; then
    sed -i "s/Z/allow ${sourceIP};\ndeny all;/g" ./$filename
fi

cd /etc/nginx/sites-enabled
ln -s ../sites-available/$filename $filename

systemctl reload nginx

echo "Port: $RANDOM_PORT / URL: http://${domainString}.${BASE_DOMAIN}/"
