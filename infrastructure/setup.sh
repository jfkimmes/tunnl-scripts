#!/bin/bash

# requires an admin user with ssh setup and sudo privs

## PLAYBOOK ##
# - setup tunnl user
#   + generate ssh key
#   + create tunnl user
#   + setup sshd for tunnl user
#   + add key to authorized_keys
#   + set up restricted shell
# - fetch ssh keys
# - install deps
# - upload scripts
# - move files to correct location
#   + move template to nginx
#   + make sure default config is enabled
#   + make scripts root owned and world readable - not writeable
#   + download private key
# - setup sudoers file -> nopasswd
# - setup 


## Script ##
# - copy private key to correct location
# ...
